# to read the text file
file = open("list1.txt", "r")
file_lines = file.read()
list_of_lines = file_lines.split()
print(list_of_lines)

#to create a consistent unique list of the file without repeatition
unique_list = list(dict.fromkeys(list_of_lines))
print(unique_list)

#to create a reverve of the unique list
unique_reverse_list =unique_list[::-1]
print(unique_reverse_list)

#to create a new text file
outF = open("list1_unique_reverse.txt", "w")
#to write in the values of the unique reverse list on the new textfile
for line in unique_reverse_list:
    outF.write(line)
    outF.write("\n")
outF.close()
