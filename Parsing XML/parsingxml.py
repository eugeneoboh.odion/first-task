from bs4 import BeautifulSoup

# Reading data from the xml file
with open('params.xml', 'r') as f:
    data = f.read()

# Passing the data of the xml
# file to the xml parser of
# beautifulsoup
bs_data = BeautifulSoup(data, 'xml')

# A loop for replacing the value
tag = bs_data.maximumCatch
#print(tag)
tag.string.replace_with("36347")
#tag.string = "5363"

# Output the contents of the
# modified xml file
print(bs_data.prettify())

# save your xml
with open("new.xml", "w") as f:
    f.write(bs_data.prettify())
